var debugging_enabled = true;
if (process.env.NODE_ENV === 'live') debugging_enabled = false;

/*
 * ----------------------
 * LOG REQUEST PARAMS
 * ----------------------
 */
exports.logRequest = function (request) {
    if (debugging_enabled) {
        console.log("REQUEST: " + JSON.stringify(request.body));
    }
};

/*
 * ----------------------
 * LOG REQUEST PARAMS
 * ----------------------
 */
exports.logGetRequest = function (request) {
    if (debugging_enabled) {
        console.log("REQUEST: " + JSON.stringify(request.query));
    }
};

/*
 * ----------------------
 * START SECTION
 * ----------------------
 */
exports.startSection = function (section) {
    if (debugging_enabled) {
        console.log("=============   " + section + "   ==============");
    }
};

/*
 * ----------------------
 * LOG RESPONSE
 * ----------------------
 */
exports.logResponse = function (response) {
    if (debugging_enabled) {
        console.log("RESPONSE: " + JSON.stringify(response, undefined, 2));
    }
};

exports.consolelog = function (eventFired, error, result) {
    if (debugging_enabled) {
        console.log(eventFired);
        if(error) console.log(error);
        if(result) console.log(result);
    }
};

exports.consoleError = function (eventFired, error, result) {
    if (debugging_enabled) {
        console.error(eventFired);
        if(error) console.error(error);
        if(result) console.error(result);
    }
};


exports.logDatabaseQueryError = function (eventFired, error, result) {
    if (debugging_enabled) {
        console.log(error)
        process.stderr.write("Event: " + eventFired);
        process.stderr.write("\tError: " + JSON.stringify(error));
        process.stderr.write("\tResult: " + JSON.stringify(result));
    }
};
