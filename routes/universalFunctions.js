


/*
 * --------------------------------------
 * CHECK EACH ELEMENT OF ARRAY FOR BLANK
 * --------------------------------------
 */
exports.checkBlank = function (arr) {
    var arrlength = arr.length;
    for (var i = 0; i < arrlength; i++) {
        if (arr[i] === undefined || arr[i] == null || arr[i] == NaN || arr[i] == 'NaN') {
            arr[i] = "";
        } else {
            arr[i] = arr[i];
        }
        arr[i] = arr[i].toString().trim();
        if (arr[i] === '' || arr[i] === "" || arr[i] === undefined || arr[i] == NaN || arr[i] == 'NaN') {
            return 1;
        }
    }
    return 0;
};

