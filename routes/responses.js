

exports.parameterMissingResponse = function (res) {
    var response = {
        "message": constants.responseMessages.PARAMETER_MISSING,
        "status": constants.responseFlags.PARAMETER_MISSING,
        "data": {}
    };
    logging.logResponse(response);
    res.send(JSON.stringify(response));
};


exports.sendErrorWithMessage = function (res, err) {
    var response = {
        "message": err,
        "status": constants.responseFlags.SHOW_ERROR,
        "data": {}
    };
    logging.logResponse(response);
    res.send(JSON.stringify(response));
};

exports.sendError = function (res) {
    var response = {
        "message": constants.responseMessages.ERROR_IN_EXECUTION,
        "status": constants.responseFlags.ERROR_IN_EXECUTION,
        "data" : {}
    };
    logging.logResponse(response);
    res.send(JSON.stringify(response));
};

exports.actionCompleteResponse = function (res, data, message) {
    var response = {
        "message": message || constants.responseMessages.ACTION_COMPLETE,
        "status": constants.responseFlags.ACTION_COMPLETE,
        "data" : data || {}
    };
    logging.logResponse(response);
    res.send(JSON.stringify(response));
};
