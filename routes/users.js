
/*
 * ------------------------------------
 * EMAIL SIGN UP.
 * ------------------------------------
 */

exports.generate_pay_slip = function (req, res) {
    res.header("Access-Control-Allow-Origin", "*");
    logging.startSection("generate_pay_slip");
    logging.logRequest(req);

    var firstName        = req.body.first_name == null || req.body.first_name == undefined ? "" : req.body.first_name;
    var lastName         = req.body.last_name == null || req.body.last_name == undefined ? "" : req.body.last_name;
    var annualSalary     = isNaN(req.body.annual_salary)  ? 0 : Math.round(req.body.annual_salary);
    var superRate        = isNaN(req.body.super_rate) ? 0 : Math.round(req.body.super_rate);
    var paymentStartDate = req.body.payment_start_date == null || req.body.payment_start_date == undefined ? "" : (req.body.payment_start_date).toString();
    var paymentEndDate   = req.body.payment_end_date == null || req.body.payment_end_date == undefined ? "" : (req.body.payment_end_date).toString();
    
    var blankData = [firstName, annualSalary, superRate];

    var output = {};
    output.fullName = firstName+' '+lastName;
    output.pay_period = paymentStartDate+' to '+paymentEndDate;
    async.waterfall([
        function (cb) {
            if (universalFunctions.checkBlank(blankData)) {
                return responses.parameterMissingResponse(res);
            } else {
                cb(null, {});
            }
        },
        function (result, cb) {
            if (inputParamsValidator.toDate(paymentStartDate) && inputParamsValidator.toDate(paymentEndDate)) {
                paymentStartDate = new Date(paymentStartDate);
                paymentEndDate = new Date(paymentEndDate);
                if(moment(paymentStartDate).isBefore(paymentEndDate)) {
                    var Start = moment(paymentStartDate);
                    var End = moment(paymentEndDate);
                    output.number_of_months = End.diff(Start, 'months') + 1;
                    cb(null, {});   
                } else {
                    return responses.sendErrorWithMessage(res, 'End date must be greater than start');
                }
            } else {
                return responses.sendErrorWithMessage(res, constants.responseMessages.INVALID_DATE);
            }
        },
        function (result, cb) {
            var taxApllicableStartDate = '07-01-2017'
            if(moment(paymentStartDate).isBefore(taxApllicableStartDate)) {
                return responses.sendErrorWithMessage(res, 'No Plan for this duration');
            } else {
                if(new Date(paymentEndDate).getFullYear() == 2018 || new Date(paymentEndDate).getFullYear() == 2017) {
                    cb(null, {});
                } else {
                    return responses.sendErrorWithMessage(res, 'No Plan for this duration');
                }
            }
        },
        function (result, cb) {
            output.gross_income_tax    = getGrossIncomeTax(annualSalary);
            output.income_tax_per_month = isNaN(output.gross_income_tax) ? 0 : Math.round((output.gross_income_tax)/12);
            cb(null, {});
        },
        function (result, cb) {
            output.gross_income = Math.round((parseInt(annualSalary)/12)) * output.number_of_months;
            output.income_tax   = output.income_tax_per_month  * output.number_of_months;
            output.net_income   = output.gross_income  - output.income_tax;
            output.super_amount = Math.round((output.gross_income  * superRate) / 100);
            console.log('output',output);
            cb(null, output);
        },
        function (result, cb) {
            var response = {
                name              : output.fullName,
                pay_period        : output.pay_period,
                number_of_months  : output.number_of_months,
                gross_income      : output.gross_income,
                income_tax        : output.income_tax,
                net_income        : output.net_income,
                super_amount      : output.super_amount,
            }
            cb(null, response)
        }
    ], function (error, result) {
        if (error) {
            return responses.sendError(res);
        } else {
            return responses.actionCompleteResponse(res, result);
        }
    });
}


function getGrossIncomeTax(annualSalary) {
    var grossIncomeTax = 0
    if(annualSalary > 18200 && annualSalary <= 37000) {
        grossIncomeTax = Math.round(((annualSalary - 18200) * 19)/100);
    } else if(annualSalary > 37000 && annualSalary <= 87000) {
        grossIncomeTax = 3572 + Math.round(((annualSalary - 37000) * 32.5)/100);
    } else if(annualSalary > 87000 && annualSalary <= 180000) {
        grossIncomeTax = 19822 + Math.round(((annualSalary - 87000) * 37)/100);
    } else if(annualSalary > 180000) {
        grossIncomeTax = 54232 + Math.round(((annualSalary - 180000) * 45)/100);
    }
    return grossIncomeTax;
}
