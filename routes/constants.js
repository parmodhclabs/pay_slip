function define(obj, name, value) {
    Object.defineProperty(obj, name, {
        value: value,
        enumerable: true,
        writable: false,
        configurable: true
    });
};


exports.responseFlags = {};
exports.responseMessages = {};


define(exports.responseMessages, 'PARAMETER_MISSING', 'Insufficient information was supplied. Please check and try again.');
define(exports.responseMessages, 'INVALID_DATE', 'Invalid date.');


//FOR FLAGS
define(exports.responseFlags, 'PARAMETER_MISSING', 100);
define(exports.responseFlags, 'SHOW_ERROR', 202);
define(exports.responseFlags, 'ERROR_IN_EXECUTION', 404);
define(exports.responseFlags, 'ACTION_COMPLETE', 200);

