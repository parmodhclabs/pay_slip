1. Clone the project :

git clone https://parmodhclabs@bitbucket.org/parmodhclabs/pay_slip.git

2. Go into the folder :
 cd pay_slips/

3. Install node in system :
 https://nodejs.org/en/download/

4. run npm install

5. Start node :
 NODE_ENV=development node app.js

6. Open a test file :
http://localhost:8888/users