/**
 * Module dependencies.
 */

process.env.NODE_CONFIG_DIR = 'config/';
// Moving NODE_APP_INSTANCE aside during configuration loading
var app_instance = process.argv.NODE_APP_INSTANCE;
process.argv.NODE_APP_INSTANCE = "";
config = require('config');
process.argv.NODE_APP_INSTANCE = app_instance;


require('pmx').init();

var express = require('express');
var path = require('path');
var bodyParser = require('body-parser');
var logger = require('morgan');

async = require('async');
inputParamsValidator = require('validator');
moment = require('moment');

logging = require('./routes/logging');
universalFunctions = require('./routes/universalFunctions');
constants = require('./routes/constants');
responses = require('./routes/responses');

var users = require('./routes/users');

var app = express();
// all environments
app.set('port',  config.get('PORT'));
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');
app.use(bodyParser.urlencoded({limit: '50mb', extended: true}));
app.use(bodyParser.json({limit: '50mb'}));
app.use(logger('dev'));
app.use(express.static(path.join(__dirname, 'public')));

app.use(function (req, res, next) {
    // Website you wish to allow to connect
    res.setHeader('Access-Control-Allow-Origin', '*');
    // Request methods you wish to allow
    res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');
    // Request headers you wish to allow
    res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,content-type');
    // Set to true if you need the website to include cookies in the requests sent
    // to the API (e.g. in case you use sessions)
    res.setHeader('Access-Control-Allow-Credentials', true);
    // Pass to next layer of middleware
    next();
});

//FOR API TESTING
app.get('/users', function (req, res) {
    res.render('users');
});


//----------------- USER'S API --------------------------
app.post('/generate_pay_slip',                   users.generate_pay_slip);

var startServer;
var http = require('http');
startServer = http.createServer(app).listen(app.get('port'), function () {
    console.log('Express server listening on port ', app.get('port'), "Env ", app.get('env'));
    // startInitialProcess();
});

process.on('message', function (message) {
    if (message === 'shutdown') {
        startServer.close();
        setTimeout(function () {
            process.exit(0);
        }, 15000);
    }
});